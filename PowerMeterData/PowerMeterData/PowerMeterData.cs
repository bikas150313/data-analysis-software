﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PowerMeterData
{
    public partial class PowerMeterData : Form
    {
        public PowerMeterData()
        {
            InitializeComponent();
        }

        private void PowerMeterData_Load(object sender, EventArgs e)
        {
            dataGridView.Columns.Add("col1", "Speed");
            dataGridView.Columns.Add("col2", "Cadence");
            dataGridView.Columns.Add("col3", "Altitude");
            dataGridView.Columns.Add("col4", "Heart Rate");
            dataGridView.Columns.Add("col5", "Power in Watts");
            string filePath = @"D:\Slideshow Lectures\sem6\ASE-B\Data for Assignment\ASE-B Data\data.txt";
            StreamReader reader = new StreamReader(filePath);
            int row = 0;
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                string[] columns = line.Split('\t');
                dataGridView.Rows.Add();
                for (int i = 0; i < columns.Length; i++)
                {
                    dataGridView[i, row].Value = columns[i];
                }
                row++;
            }
            /*while ((line = reader.ReadLine()) != null)
            {
                string[] columns = line.Split('\t');
                dataGridView.Rows.Add();
                for (int i = 0; i < columns.Length; i++)
                {
                    dataGridView[i, row].Value = columns[i];
                }
                row++;
            }*/
        }
    }
}
