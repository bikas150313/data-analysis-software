﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace DataAnalysisSoftware
{
    public partial class Graph : Form
    {

        //variable declaration
        //can be used everywhere in this class
        int[] heartRateArray;
        int[] speedArray;
        int[] cadenceArray;
        int[] altitudeArray;
        int[] powerInWattsArray;

        //parameterized constructor
        public Graph(int[] heartRateArray, int[] speedArray, int[] cadenceArray, 
            int[] altitudeArray, int[] powerInWattsArray)
        {
            this.heartRateArray = heartRateArray;
            this.speedArray = speedArray;
            this.cadenceArray = cadenceArray;
            this.altitudeArray = altitudeArray;
            this.powerInWattsArray = powerInWattsArray;
            InitializeComponent();
            //method calling
            PlotGraph();
        }

        //method to plot graph
        public void PlotGraph()
        {
            //graph pane for displaying the elements 
            //associated with an individual graph
            GraphPane graphPane = zedGraphControl.GraphPane;
            //graph and axis titles
            graphPane.Title = "Cycle Computer Data Graph";
            graphPane.XAxis.Title = "Time Interval in Seconds";
            graphPane.YAxis.Title = "Value";
            //y-axiz min and max value
            graphPane.YAxis.Min = 0;
            graphPane.YAxis.Max = 1100;
            //pointpairlist to determine X and Y cordinates
            PointPairList heartRatePairList = new PointPairList();
            PointPairList speedPairList = new PointPairList();
            PointPairList cadencePairList = new PointPairList();
            PointPairList altitudePairList = new PointPairList();
            PointPairList powerPairList = new PointPairList();
            int totalItems = heartRateArray.Length;
            for (int i = 0; i < totalItems; i++)
            {
                heartRatePairList.Add(i, heartRateArray[i]);
                speedPairList.Add(i, speedArray[i]);
                cadencePairList.Add(i, cadenceArray[i]);
                altitudePairList.Add(i, altitudeArray[i]);
                powerPairList.Add(i, powerInWattsArray[i]);
            }
            //creating line item and adding to graph pane
            LineItem heartRateCurve = graphPane.AddCurve("Heart Rate",
                   heartRatePairList, Color.Red, SymbolType.None);
            LineItem speedCurve = graphPane.AddCurve("Speed",
                  speedPairList, Color.Blue, SymbolType.None);
            LineItem cadenceCurve = graphPane.AddCurve("Cadence",
                   cadencePairList, Color.Black, SymbolType.None);
            LineItem altitudeCurve = graphPane.AddCurve("Altitude",
                  altitudePairList, Color.Brown, SymbolType.None);
            LineItem powerCurve = graphPane.AddCurve("Power",
                  powerPairList, Color.Green, SymbolType.None);
            //for auto scaling operation
            zedGraphControl.AxisChange();
        }
    }
}
