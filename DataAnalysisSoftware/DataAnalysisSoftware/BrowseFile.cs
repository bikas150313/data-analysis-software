﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataAnalysisSoftware
{
    public partial class BrowseFile : Form
    {
        public BrowseFile()
        {
            InitializeComponent();
        }

        private void btn_browse_Click(object sender, EventArgs e)
        {
            //open file dialog to select data file
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Filter = "HRM file (*.hrm) | *.hrm";
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                string fileName = fileDialog.FileName;
                this.Hide();
                //creating and calling dashboard constructor object
                //passing file name to dashboard
                Dashboard dashboard = new Dashboard(fileName);
                dashboard.ShowDialog();
                this.Close();
            }
        }
    }
}
