﻿namespace DataAnalysisSoftware
{
    partial class Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Dashboard));
            this.pan_fileInformation = new System.Windows.Forms.Panel();
            this.groupBox_summaryData = new System.Windows.Forms.GroupBox();
            this.lbl_totalDistanceValue = new System.Windows.Forms.Label();
            this.lbl_totalDistance = new System.Windows.Forms.Label();
            this.lbl_maxAltitudeValue = new System.Windows.Forms.Label();
            this.lbl_avgAltitudeValue = new System.Windows.Forms.Label();
            this.lbl_maxPowerValue = new System.Windows.Forms.Label();
            this.lbl_avgPowerValue = new System.Windows.Forms.Label();
            this.lbl_minHeartRateValue = new System.Windows.Forms.Label();
            this.lbl_maxHeartRateValue = new System.Windows.Forms.Label();
            this.lbl_avgHeartRateValue = new System.Windows.Forms.Label();
            this.lbl_maxSpeedValue = new System.Windows.Forms.Label();
            this.lbl_avgSpeedValue = new System.Windows.Forms.Label();
            this.lbl_maxAltitude = new System.Windows.Forms.Label();
            this.lbl_avgAltitude = new System.Windows.Forms.Label();
            this.lbl_maxPower = new System.Windows.Forms.Label();
            this.lbl_avgPower = new System.Windows.Forms.Label();
            this.lbl_minHeartRate = new System.Windows.Forms.Label();
            this.lbl_maxHeartRate = new System.Windows.Forms.Label();
            this.lbl_avgHeartRate = new System.Windows.Forms.Label();
            this.lbl_maxSpeed = new System.Windows.Forms.Label();
            this.lbl_avgSpeed = new System.Windows.Forms.Label();
            this.groupBox_speedUnit = new System.Windows.Forms.GroupBox();
            this.radioButton_milePerHour = new System.Windows.Forms.RadioButton();
            this.radioButton_kmPerHour = new System.Windows.Forms.RadioButton();
            this.groupBox_headerData = new System.Windows.Forms.GroupBox();
            this.lbl_intervalValue = new System.Windows.Forms.Label();
            this.lbl_lengthValue = new System.Windows.Forms.Label();
            this.lbl_startTimeValue = new System.Windows.Forms.Label();
            this.lbl_dateValue = new System.Windows.Forms.Label();
            this.lbl_sModeValue = new System.Windows.Forms.Label();
            this.lbl_versionValue = new System.Windows.Forms.Label();
            this.lbl_interval = new System.Windows.Forms.Label();
            this.lbl_length = new System.Windows.Forms.Label();
            this.lbl_startTime = new System.Windows.Forms.Label();
            this.lbl_date = new System.Windows.Forms.Label();
            this.lbl_sMode = new System.Windows.Forms.Label();
            this.lbl_version = new System.Windows.Forms.Label();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.HeartRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Speed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cadence = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Altitude = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PowerInWatts = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PowerBalanceAndPedallingIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pan_createButton = new System.Windows.Forms.Panel();
            this.btn_graphReport = new System.Windows.Forms.Button();
            this.groupBox_sMode = new System.Windows.Forms.GroupBox();
            this.lbl_speed = new System.Windows.Forms.Label();
            this.txt_speed = new System.Windows.Forms.TextBox();
            this.txt_cadence = new System.Windows.Forms.TextBox();
            this.lbl_cadence = new System.Windows.Forms.Label();
            this.txt_altitude = new System.Windows.Forms.TextBox();
            this.lbl_altitude = new System.Windows.Forms.Label();
            this.txt_power = new System.Windows.Forms.TextBox();
            this.lbl_power = new System.Windows.Forms.Label();
            this.txt_powerIndex = new System.Windows.Forms.TextBox();
            this.lbl_powerIndex = new System.Windows.Forms.Label();
            this.pan_fileInformation.SuspendLayout();
            this.groupBox_summaryData.SuspendLayout();
            this.groupBox_speedUnit.SuspendLayout();
            this.groupBox_headerData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.pan_createButton.SuspendLayout();
            this.groupBox_sMode.SuspendLayout();
            this.SuspendLayout();
            // 
            // pan_fileInformation
            // 
            this.pan_fileInformation.BackColor = System.Drawing.Color.LightCyan;
            this.pan_fileInformation.Controls.Add(this.groupBox_sMode);
            this.pan_fileInformation.Controls.Add(this.groupBox_summaryData);
            this.pan_fileInformation.Controls.Add(this.groupBox_speedUnit);
            this.pan_fileInformation.Controls.Add(this.groupBox_headerData);
            this.pan_fileInformation.Location = new System.Drawing.Point(829, 0);
            this.pan_fileInformation.Name = "pan_fileInformation";
            this.pan_fileInformation.Size = new System.Drawing.Size(523, 702);
            this.pan_fileInformation.TabIndex = 1;
            // 
            // groupBox_summaryData
            // 
            this.groupBox_summaryData.BackColor = System.Drawing.Color.White;
            this.groupBox_summaryData.Controls.Add(this.lbl_totalDistanceValue);
            this.groupBox_summaryData.Controls.Add(this.lbl_totalDistance);
            this.groupBox_summaryData.Controls.Add(this.lbl_maxAltitudeValue);
            this.groupBox_summaryData.Controls.Add(this.lbl_avgAltitudeValue);
            this.groupBox_summaryData.Controls.Add(this.lbl_maxPowerValue);
            this.groupBox_summaryData.Controls.Add(this.lbl_avgPowerValue);
            this.groupBox_summaryData.Controls.Add(this.lbl_minHeartRateValue);
            this.groupBox_summaryData.Controls.Add(this.lbl_maxHeartRateValue);
            this.groupBox_summaryData.Controls.Add(this.lbl_avgHeartRateValue);
            this.groupBox_summaryData.Controls.Add(this.lbl_maxSpeedValue);
            this.groupBox_summaryData.Controls.Add(this.lbl_avgSpeedValue);
            this.groupBox_summaryData.Controls.Add(this.lbl_maxAltitude);
            this.groupBox_summaryData.Controls.Add(this.lbl_avgAltitude);
            this.groupBox_summaryData.Controls.Add(this.lbl_maxPower);
            this.groupBox_summaryData.Controls.Add(this.lbl_avgPower);
            this.groupBox_summaryData.Controls.Add(this.lbl_minHeartRate);
            this.groupBox_summaryData.Controls.Add(this.lbl_maxHeartRate);
            this.groupBox_summaryData.Controls.Add(this.lbl_avgHeartRate);
            this.groupBox_summaryData.Controls.Add(this.lbl_maxSpeed);
            this.groupBox_summaryData.Controls.Add(this.lbl_avgSpeed);
            this.groupBox_summaryData.Font = new System.Drawing.Font("Consolas", 14.25F);
            this.groupBox_summaryData.Location = new System.Drawing.Point(14, 249);
            this.groupBox_summaryData.Name = "groupBox_summaryData";
            this.groupBox_summaryData.Size = new System.Drawing.Size(362, 444);
            this.groupBox_summaryData.TabIndex = 4;
            this.groupBox_summaryData.TabStop = false;
            this.groupBox_summaryData.Text = "Summary Data";
            // 
            // lbl_totalDistanceValue
            // 
            this.lbl_totalDistanceValue.AutoSize = true;
            this.lbl_totalDistanceValue.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_totalDistanceValue.ForeColor = System.Drawing.Color.Black;
            this.lbl_totalDistanceValue.Location = new System.Drawing.Point(237, 52);
            this.lbl_totalDistanceValue.Name = "lbl_totalDistanceValue";
            this.lbl_totalDistanceValue.Size = new System.Drawing.Size(0, 19);
            this.lbl_totalDistanceValue.TabIndex = 31;
            // 
            // lbl_totalDistance
            // 
            this.lbl_totalDistance.AutoSize = true;
            this.lbl_totalDistance.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_totalDistance.ForeColor = System.Drawing.Color.Blue;
            this.lbl_totalDistance.Location = new System.Drawing.Point(6, 52);
            this.lbl_totalDistance.Name = "lbl_totalDistance";
            this.lbl_totalDistance.Size = new System.Drawing.Size(225, 19);
            this.lbl_totalDistance.TabIndex = 30;
            this.lbl_totalDistance.Text = "Total Distance Covered :";
            // 
            // lbl_maxAltitudeValue
            // 
            this.lbl_maxAltitudeValue.AutoSize = true;
            this.lbl_maxAltitudeValue.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_maxAltitudeValue.ForeColor = System.Drawing.Color.Black;
            this.lbl_maxAltitudeValue.Location = new System.Drawing.Point(237, 390);
            this.lbl_maxAltitudeValue.Name = "lbl_maxAltitudeValue";
            this.lbl_maxAltitudeValue.Size = new System.Drawing.Size(0, 19);
            this.lbl_maxAltitudeValue.TabIndex = 29;
            // 
            // lbl_avgAltitudeValue
            // 
            this.lbl_avgAltitudeValue.AutoSize = true;
            this.lbl_avgAltitudeValue.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_avgAltitudeValue.ForeColor = System.Drawing.Color.Black;
            this.lbl_avgAltitudeValue.Location = new System.Drawing.Point(237, 352);
            this.lbl_avgAltitudeValue.Name = "lbl_avgAltitudeValue";
            this.lbl_avgAltitudeValue.Size = new System.Drawing.Size(0, 19);
            this.lbl_avgAltitudeValue.TabIndex = 28;
            // 
            // lbl_maxPowerValue
            // 
            this.lbl_maxPowerValue.AutoSize = true;
            this.lbl_maxPowerValue.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_maxPowerValue.ForeColor = System.Drawing.Color.Black;
            this.lbl_maxPowerValue.Location = new System.Drawing.Point(237, 313);
            this.lbl_maxPowerValue.Name = "lbl_maxPowerValue";
            this.lbl_maxPowerValue.Size = new System.Drawing.Size(0, 19);
            this.lbl_maxPowerValue.TabIndex = 27;
            // 
            // lbl_avgPowerValue
            // 
            this.lbl_avgPowerValue.AutoSize = true;
            this.lbl_avgPowerValue.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_avgPowerValue.ForeColor = System.Drawing.Color.Black;
            this.lbl_avgPowerValue.Location = new System.Drawing.Point(237, 275);
            this.lbl_avgPowerValue.Name = "lbl_avgPowerValue";
            this.lbl_avgPowerValue.Size = new System.Drawing.Size(0, 19);
            this.lbl_avgPowerValue.TabIndex = 26;
            // 
            // lbl_minHeartRateValue
            // 
            this.lbl_minHeartRateValue.AutoSize = true;
            this.lbl_minHeartRateValue.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_minHeartRateValue.ForeColor = System.Drawing.Color.Black;
            this.lbl_minHeartRateValue.Location = new System.Drawing.Point(237, 236);
            this.lbl_minHeartRateValue.Name = "lbl_minHeartRateValue";
            this.lbl_minHeartRateValue.Size = new System.Drawing.Size(0, 19);
            this.lbl_minHeartRateValue.TabIndex = 25;
            // 
            // lbl_maxHeartRateValue
            // 
            this.lbl_maxHeartRateValue.AutoSize = true;
            this.lbl_maxHeartRateValue.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_maxHeartRateValue.ForeColor = System.Drawing.Color.Black;
            this.lbl_maxHeartRateValue.Location = new System.Drawing.Point(237, 200);
            this.lbl_maxHeartRateValue.Name = "lbl_maxHeartRateValue";
            this.lbl_maxHeartRateValue.Size = new System.Drawing.Size(0, 19);
            this.lbl_maxHeartRateValue.TabIndex = 24;
            // 
            // lbl_avgHeartRateValue
            // 
            this.lbl_avgHeartRateValue.AutoSize = true;
            this.lbl_avgHeartRateValue.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_avgHeartRateValue.ForeColor = System.Drawing.Color.Black;
            this.lbl_avgHeartRateValue.Location = new System.Drawing.Point(237, 163);
            this.lbl_avgHeartRateValue.Name = "lbl_avgHeartRateValue";
            this.lbl_avgHeartRateValue.Size = new System.Drawing.Size(0, 19);
            this.lbl_avgHeartRateValue.TabIndex = 23;
            // 
            // lbl_maxSpeedValue
            // 
            this.lbl_maxSpeedValue.AutoSize = true;
            this.lbl_maxSpeedValue.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_maxSpeedValue.ForeColor = System.Drawing.Color.Black;
            this.lbl_maxSpeedValue.Location = new System.Drawing.Point(237, 125);
            this.lbl_maxSpeedValue.Name = "lbl_maxSpeedValue";
            this.lbl_maxSpeedValue.Size = new System.Drawing.Size(0, 19);
            this.lbl_maxSpeedValue.TabIndex = 22;
            // 
            // lbl_avgSpeedValue
            // 
            this.lbl_avgSpeedValue.AutoSize = true;
            this.lbl_avgSpeedValue.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_avgSpeedValue.ForeColor = System.Drawing.Color.Black;
            this.lbl_avgSpeedValue.Location = new System.Drawing.Point(237, 87);
            this.lbl_avgSpeedValue.Name = "lbl_avgSpeedValue";
            this.lbl_avgSpeedValue.Size = new System.Drawing.Size(0, 19);
            this.lbl_avgSpeedValue.TabIndex = 21;
            // 
            // lbl_maxAltitude
            // 
            this.lbl_maxAltitude.AutoSize = true;
            this.lbl_maxAltitude.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_maxAltitude.ForeColor = System.Drawing.Color.Blue;
            this.lbl_maxAltitude.Location = new System.Drawing.Point(6, 390);
            this.lbl_maxAltitude.Name = "lbl_maxAltitude";
            this.lbl_maxAltitude.Size = new System.Drawing.Size(171, 19);
            this.lbl_maxAltitude.TabIndex = 20;
            this.lbl_maxAltitude.Text = "Maximum Altitude :";
            // 
            // lbl_avgAltitude
            // 
            this.lbl_avgAltitude.AutoSize = true;
            this.lbl_avgAltitude.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_avgAltitude.ForeColor = System.Drawing.Color.Blue;
            this.lbl_avgAltitude.Location = new System.Drawing.Point(6, 352);
            this.lbl_avgAltitude.Name = "lbl_avgAltitude";
            this.lbl_avgAltitude.Size = new System.Drawing.Size(171, 19);
            this.lbl_avgAltitude.TabIndex = 19;
            this.lbl_avgAltitude.Text = "Average Altitude :";
            // 
            // lbl_maxPower
            // 
            this.lbl_maxPower.AutoSize = true;
            this.lbl_maxPower.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_maxPower.ForeColor = System.Drawing.Color.Blue;
            this.lbl_maxPower.Location = new System.Drawing.Point(6, 313);
            this.lbl_maxPower.Name = "lbl_maxPower";
            this.lbl_maxPower.Size = new System.Drawing.Size(144, 19);
            this.lbl_maxPower.TabIndex = 18;
            this.lbl_maxPower.Text = "Maximum Power :";
            // 
            // lbl_avgPower
            // 
            this.lbl_avgPower.AutoSize = true;
            this.lbl_avgPower.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_avgPower.ForeColor = System.Drawing.Color.Blue;
            this.lbl_avgPower.Location = new System.Drawing.Point(6, 275);
            this.lbl_avgPower.Name = "lbl_avgPower";
            this.lbl_avgPower.Size = new System.Drawing.Size(144, 19);
            this.lbl_avgPower.TabIndex = 17;
            this.lbl_avgPower.Text = "Average Power :";
            // 
            // lbl_minHeartRate
            // 
            this.lbl_minHeartRate.AutoSize = true;
            this.lbl_minHeartRate.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_minHeartRate.ForeColor = System.Drawing.Color.Blue;
            this.lbl_minHeartRate.Location = new System.Drawing.Point(6, 236);
            this.lbl_minHeartRate.Name = "lbl_minHeartRate";
            this.lbl_minHeartRate.Size = new System.Drawing.Size(189, 19);
            this.lbl_minHeartRate.TabIndex = 16;
            this.lbl_minHeartRate.Text = "Minimum Heart Rate :";
            // 
            // lbl_maxHeartRate
            // 
            this.lbl_maxHeartRate.AutoSize = true;
            this.lbl_maxHeartRate.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_maxHeartRate.ForeColor = System.Drawing.Color.Blue;
            this.lbl_maxHeartRate.Location = new System.Drawing.Point(6, 200);
            this.lbl_maxHeartRate.Name = "lbl_maxHeartRate";
            this.lbl_maxHeartRate.Size = new System.Drawing.Size(189, 19);
            this.lbl_maxHeartRate.TabIndex = 15;
            this.lbl_maxHeartRate.Text = "Maximum Heart Rate :";
            // 
            // lbl_avgHeartRate
            // 
            this.lbl_avgHeartRate.AutoSize = true;
            this.lbl_avgHeartRate.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_avgHeartRate.ForeColor = System.Drawing.Color.Blue;
            this.lbl_avgHeartRate.Location = new System.Drawing.Point(6, 163);
            this.lbl_avgHeartRate.Name = "lbl_avgHeartRate";
            this.lbl_avgHeartRate.Size = new System.Drawing.Size(189, 19);
            this.lbl_avgHeartRate.TabIndex = 14;
            this.lbl_avgHeartRate.Text = "Average Heart Rate :";
            // 
            // lbl_maxSpeed
            // 
            this.lbl_maxSpeed.AutoSize = true;
            this.lbl_maxSpeed.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_maxSpeed.ForeColor = System.Drawing.Color.Blue;
            this.lbl_maxSpeed.Location = new System.Drawing.Point(6, 125);
            this.lbl_maxSpeed.Name = "lbl_maxSpeed";
            this.lbl_maxSpeed.Size = new System.Drawing.Size(144, 19);
            this.lbl_maxSpeed.TabIndex = 13;
            this.lbl_maxSpeed.Text = "Maximum Speed :";
            // 
            // lbl_avgSpeed
            // 
            this.lbl_avgSpeed.AutoSize = true;
            this.lbl_avgSpeed.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_avgSpeed.ForeColor = System.Drawing.Color.Blue;
            this.lbl_avgSpeed.Location = new System.Drawing.Point(6, 87);
            this.lbl_avgSpeed.Name = "lbl_avgSpeed";
            this.lbl_avgSpeed.Size = new System.Drawing.Size(144, 19);
            this.lbl_avgSpeed.TabIndex = 12;
            this.lbl_avgSpeed.Text = "Average Speed :";
            // 
            // groupBox_speedUnit
            // 
            this.groupBox_speedUnit.BackColor = System.Drawing.Color.White;
            this.groupBox_speedUnit.Controls.Add(this.radioButton_milePerHour);
            this.groupBox_speedUnit.Controls.Add(this.radioButton_kmPerHour);
            this.groupBox_speedUnit.Font = new System.Drawing.Font("Consolas", 14.25F);
            this.groupBox_speedUnit.Location = new System.Drawing.Point(14, 174);
            this.groupBox_speedUnit.Name = "groupBox_speedUnit";
            this.groupBox_speedUnit.Size = new System.Drawing.Size(497, 69);
            this.groupBox_speedUnit.TabIndex = 3;
            this.groupBox_speedUnit.TabStop = false;
            this.groupBox_speedUnit.Text = "Speed Unit";
            // 
            // radioButton_milePerHour
            // 
            this.radioButton_milePerHour.AutoSize = true;
            this.radioButton_milePerHour.Font = new System.Drawing.Font("Consolas", 12F);
            this.radioButton_milePerHour.ForeColor = System.Drawing.Color.Blue;
            this.radioButton_milePerHour.Location = new System.Drawing.Point(294, 29);
            this.radioButton_milePerHour.Name = "radioButton_milePerHour";
            this.radioButton_milePerHour.Size = new System.Drawing.Size(54, 23);
            this.radioButton_milePerHour.TabIndex = 4;
            this.radioButton_milePerHour.Text = "mph";
            this.radioButton_milePerHour.UseVisualStyleBackColor = true;
            this.radioButton_milePerHour.Click += new System.EventHandler(this.radioButton_milePerHour_Click);
            // 
            // radioButton_kmPerHour
            // 
            this.radioButton_kmPerHour.AutoSize = true;
            this.radioButton_kmPerHour.Checked = true;
            this.radioButton_kmPerHour.Enabled = false;
            this.radioButton_kmPerHour.Font = new System.Drawing.Font("Consolas", 12F);
            this.radioButton_kmPerHour.ForeColor = System.Drawing.Color.Blue;
            this.radioButton_kmPerHour.Location = new System.Drawing.Point(140, 29);
            this.radioButton_kmPerHour.Name = "radioButton_kmPerHour";
            this.radioButton_kmPerHour.Size = new System.Drawing.Size(63, 23);
            this.radioButton_kmPerHour.TabIndex = 3;
            this.radioButton_kmPerHour.TabStop = true;
            this.radioButton_kmPerHour.Text = "km/h";
            this.radioButton_kmPerHour.UseVisualStyleBackColor = true;
            this.radioButton_kmPerHour.Click += new System.EventHandler(this.radioButton_kmPerHour_Click);
            // 
            // groupBox_headerData
            // 
            this.groupBox_headerData.BackColor = System.Drawing.Color.White;
            this.groupBox_headerData.Controls.Add(this.lbl_intervalValue);
            this.groupBox_headerData.Controls.Add(this.lbl_lengthValue);
            this.groupBox_headerData.Controls.Add(this.lbl_startTimeValue);
            this.groupBox_headerData.Controls.Add(this.lbl_dateValue);
            this.groupBox_headerData.Controls.Add(this.lbl_sModeValue);
            this.groupBox_headerData.Controls.Add(this.lbl_versionValue);
            this.groupBox_headerData.Controls.Add(this.lbl_interval);
            this.groupBox_headerData.Controls.Add(this.lbl_length);
            this.groupBox_headerData.Controls.Add(this.lbl_startTime);
            this.groupBox_headerData.Controls.Add(this.lbl_date);
            this.groupBox_headerData.Controls.Add(this.lbl_sMode);
            this.groupBox_headerData.Controls.Add(this.lbl_version);
            this.groupBox_headerData.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox_headerData.Location = new System.Drawing.Point(14, 12);
            this.groupBox_headerData.Name = "groupBox_headerData";
            this.groupBox_headerData.Size = new System.Drawing.Size(497, 156);
            this.groupBox_headerData.TabIndex = 0;
            this.groupBox_headerData.TabStop = false;
            this.groupBox_headerData.Text = "Header Data";
            // 
            // lbl_intervalValue
            // 
            this.lbl_intervalValue.AutoSize = true;
            this.lbl_intervalValue.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_intervalValue.Location = new System.Drawing.Point(381, 119);
            this.lbl_intervalValue.Name = "lbl_intervalValue";
            this.lbl_intervalValue.Size = new System.Drawing.Size(0, 19);
            this.lbl_intervalValue.TabIndex = 11;
            // 
            // lbl_lengthValue
            // 
            this.lbl_lengthValue.AutoSize = true;
            this.lbl_lengthValue.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_lengthValue.Location = new System.Drawing.Point(381, 73);
            this.lbl_lengthValue.Name = "lbl_lengthValue";
            this.lbl_lengthValue.Size = new System.Drawing.Size(0, 19);
            this.lbl_lengthValue.TabIndex = 10;
            // 
            // lbl_startTimeValue
            // 
            this.lbl_startTimeValue.AutoSize = true;
            this.lbl_startTimeValue.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_startTimeValue.Location = new System.Drawing.Point(381, 32);
            this.lbl_startTimeValue.Name = "lbl_startTimeValue";
            this.lbl_startTimeValue.Size = new System.Drawing.Size(0, 19);
            this.lbl_startTimeValue.TabIndex = 9;
            // 
            // lbl_dateValue
            // 
            this.lbl_dateValue.AutoSize = true;
            this.lbl_dateValue.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_dateValue.Location = new System.Drawing.Point(122, 119);
            this.lbl_dateValue.Name = "lbl_dateValue";
            this.lbl_dateValue.Size = new System.Drawing.Size(0, 19);
            this.lbl_dateValue.TabIndex = 8;
            // 
            // lbl_sModeValue
            // 
            this.lbl_sModeValue.AutoSize = true;
            this.lbl_sModeValue.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_sModeValue.Location = new System.Drawing.Point(122, 73);
            this.lbl_sModeValue.Name = "lbl_sModeValue";
            this.lbl_sModeValue.Size = new System.Drawing.Size(0, 19);
            this.lbl_sModeValue.TabIndex = 7;
            // 
            // lbl_versionValue
            // 
            this.lbl_versionValue.AutoSize = true;
            this.lbl_versionValue.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_versionValue.Location = new System.Drawing.Point(122, 32);
            this.lbl_versionValue.Name = "lbl_versionValue";
            this.lbl_versionValue.Size = new System.Drawing.Size(0, 19);
            this.lbl_versionValue.TabIndex = 6;
            // 
            // lbl_interval
            // 
            this.lbl_interval.AutoSize = true;
            this.lbl_interval.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_interval.ForeColor = System.Drawing.Color.Blue;
            this.lbl_interval.Location = new System.Drawing.Point(245, 119);
            this.lbl_interval.Name = "lbl_interval";
            this.lbl_interval.Size = new System.Drawing.Size(99, 19);
            this.lbl_interval.TabIndex = 5;
            this.lbl_interval.Text = "Interval :";
            // 
            // lbl_length
            // 
            this.lbl_length.AutoSize = true;
            this.lbl_length.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_length.ForeColor = System.Drawing.Color.Blue;
            this.lbl_length.Location = new System.Drawing.Point(245, 73);
            this.lbl_length.Name = "lbl_length";
            this.lbl_length.Size = new System.Drawing.Size(81, 19);
            this.lbl_length.TabIndex = 4;
            this.lbl_length.Text = "Length :";
            // 
            // lbl_startTime
            // 
            this.lbl_startTime.AutoSize = true;
            this.lbl_startTime.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_startTime.ForeColor = System.Drawing.Color.Blue;
            this.lbl_startTime.Location = new System.Drawing.Point(245, 32);
            this.lbl_startTime.Name = "lbl_startTime";
            this.lbl_startTime.Size = new System.Drawing.Size(117, 19);
            this.lbl_startTime.TabIndex = 3;
            this.lbl_startTime.Text = "Start Time :";
            // 
            // lbl_date
            // 
            this.lbl_date.AutoSize = true;
            this.lbl_date.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_date.ForeColor = System.Drawing.Color.Blue;
            this.lbl_date.Location = new System.Drawing.Point(6, 119);
            this.lbl_date.Name = "lbl_date";
            this.lbl_date.Size = new System.Drawing.Size(63, 19);
            this.lbl_date.TabIndex = 2;
            this.lbl_date.Text = "Date :";
            // 
            // lbl_sMode
            // 
            this.lbl_sMode.AutoSize = true;
            this.lbl_sMode.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_sMode.ForeColor = System.Drawing.Color.Blue;
            this.lbl_sMode.Location = new System.Drawing.Point(6, 73);
            this.lbl_sMode.Name = "lbl_sMode";
            this.lbl_sMode.Size = new System.Drawing.Size(72, 19);
            this.lbl_sMode.TabIndex = 1;
            this.lbl_sMode.Text = "SMode :";
            // 
            // lbl_version
            // 
            this.lbl_version.AutoSize = true;
            this.lbl_version.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_version.ForeColor = System.Drawing.Color.Blue;
            this.lbl_version.Location = new System.Drawing.Point(6, 32);
            this.lbl_version.Name = "lbl_version";
            this.lbl_version.Size = new System.Drawing.Size(99, 19);
            this.lbl_version.TabIndex = 0;
            this.lbl_version.Text = "Version : ";
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.HeartRate,
            this.Speed,
            this.Cadence,
            this.Altitude,
            this.PowerInWatts,
            this.PowerBalanceAndPedallingIndex,
            this.Time});
            this.dataGridView.Location = new System.Drawing.Point(0, 85);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView.Size = new System.Drawing.Size(823, 617);
            this.dataGridView.TabIndex = 2;
            // 
            // HeartRate
            // 
            this.HeartRate.HeaderText = "Heart Rates (bpm) ";
            this.HeartRate.Name = "HeartRate";
            this.HeartRate.ReadOnly = true;
            // 
            // Speed
            // 
            this.Speed.HeaderText = "Speed (km/h or mph)";
            this.Speed.Name = "Speed";
            this.Speed.ReadOnly = true;
            // 
            // Cadence
            // 
            this.Cadence.HeaderText = "Cadence (rpm)";
            this.Cadence.Name = "Cadence";
            this.Cadence.ReadOnly = true;
            // 
            // Altitude
            // 
            this.Altitude.HeaderText = "Altitude (m/ft)";
            this.Altitude.Name = "Altitude";
            this.Altitude.ReadOnly = true;
            // 
            // PowerInWatts
            // 
            this.PowerInWatts.HeaderText = "Power (watts)";
            this.PowerInWatts.Name = "PowerInWatts";
            this.PowerInWatts.ReadOnly = true;
            // 
            // PowerBalanceAndPedallingIndex
            // 
            this.PowerBalanceAndPedallingIndex.HeaderText = "Power Balance and Pedalling Index";
            this.PowerBalanceAndPedallingIndex.Name = "PowerBalanceAndPedallingIndex";
            this.PowerBalanceAndPedallingIndex.ReadOnly = true;
            this.PowerBalanceAndPedallingIndex.Width = 150;
            // 
            // Time
            // 
            this.Time.HeaderText = "Time";
            this.Time.Name = "Time";
            this.Time.ReadOnly = true;
            this.Time.Width = 128;
            // 
            // pan_createButton
            // 
            this.pan_createButton.BackColor = System.Drawing.Color.Black;
            this.pan_createButton.Controls.Add(this.btn_graphReport);
            this.pan_createButton.Location = new System.Drawing.Point(0, 0);
            this.pan_createButton.Name = "pan_createButton";
            this.pan_createButton.Size = new System.Drawing.Size(823, 79);
            this.pan_createButton.TabIndex = 4;
            // 
            // btn_graphReport
            // 
            this.btn_graphReport.Font = new System.Drawing.Font("Algerian", 15.75F);
            this.btn_graphReport.ForeColor = System.Drawing.Color.Blue;
            this.btn_graphReport.Image = ((System.Drawing.Image)(resources.GetObject("btn_graphReport.Image")));
            this.btn_graphReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_graphReport.Location = new System.Drawing.Point(610, 12);
            this.btn_graphReport.Name = "btn_graphReport";
            this.btn_graphReport.Size = new System.Drawing.Size(196, 51);
            this.btn_graphReport.TabIndex = 0;
            this.btn_graphReport.Text = "Graph Report";
            this.btn_graphReport.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_graphReport.UseVisualStyleBackColor = true;
            this.btn_graphReport.Click += new System.EventHandler(this.btn_graphReport_Click);
            // 
            // groupBox_sMode
            // 
            this.groupBox_sMode.BackColor = System.Drawing.Color.White;
            this.groupBox_sMode.Controls.Add(this.txt_powerIndex);
            this.groupBox_sMode.Controls.Add(this.lbl_powerIndex);
            this.groupBox_sMode.Controls.Add(this.txt_power);
            this.groupBox_sMode.Controls.Add(this.lbl_power);
            this.groupBox_sMode.Controls.Add(this.txt_altitude);
            this.groupBox_sMode.Controls.Add(this.lbl_altitude);
            this.groupBox_sMode.Controls.Add(this.txt_cadence);
            this.groupBox_sMode.Controls.Add(this.lbl_cadence);
            this.groupBox_sMode.Controls.Add(this.txt_speed);
            this.groupBox_sMode.Controls.Add(this.lbl_speed);
            this.groupBox_sMode.Font = new System.Drawing.Font("Consolas", 14.25F);
            this.groupBox_sMode.Location = new System.Drawing.Point(382, 249);
            this.groupBox_sMode.Name = "groupBox_sMode";
            this.groupBox_sMode.Size = new System.Drawing.Size(129, 444);
            this.groupBox_sMode.TabIndex = 32;
            this.groupBox_sMode.TabStop = false;
            this.groupBox_sMode.Text = "SMode";
            // 
            // lbl_speed
            // 
            this.lbl_speed.AutoSize = true;
            this.lbl_speed.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_speed.ForeColor = System.Drawing.Color.Blue;
            this.lbl_speed.Location = new System.Drawing.Point(6, 52);
            this.lbl_speed.Name = "lbl_speed";
            this.lbl_speed.Size = new System.Drawing.Size(72, 19);
            this.lbl_speed.TabIndex = 32;
            this.lbl_speed.Text = "Speed :";
            // 
            // txt_speed
            // 
            this.txt_speed.Enabled = false;
            this.txt_speed.Location = new System.Drawing.Point(10, 76);
            this.txt_speed.Name = "txt_speed";
            this.txt_speed.Size = new System.Drawing.Size(106, 30);
            this.txt_speed.TabIndex = 33;
            // 
            // txt_cadence
            // 
            this.txt_cadence.Enabled = false;
            this.txt_cadence.Location = new System.Drawing.Point(10, 149);
            this.txt_cadence.Name = "txt_cadence";
            this.txt_cadence.Size = new System.Drawing.Size(106, 30);
            this.txt_cadence.TabIndex = 35;
            // 
            // lbl_cadence
            // 
            this.lbl_cadence.AutoSize = true;
            this.lbl_cadence.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cadence.ForeColor = System.Drawing.Color.Blue;
            this.lbl_cadence.Location = new System.Drawing.Point(6, 125);
            this.lbl_cadence.Name = "lbl_cadence";
            this.lbl_cadence.Size = new System.Drawing.Size(90, 19);
            this.lbl_cadence.TabIndex = 34;
            this.lbl_cadence.Text = "Cadence :";
            // 
            // txt_altitude
            // 
            this.txt_altitude.Enabled = false;
            this.txt_altitude.Location = new System.Drawing.Point(10, 224);
            this.txt_altitude.Name = "txt_altitude";
            this.txt_altitude.Size = new System.Drawing.Size(106, 30);
            this.txt_altitude.TabIndex = 37;
            // 
            // lbl_altitude
            // 
            this.lbl_altitude.AutoSize = true;
            this.lbl_altitude.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_altitude.ForeColor = System.Drawing.Color.Blue;
            this.lbl_altitude.Location = new System.Drawing.Point(6, 200);
            this.lbl_altitude.Name = "lbl_altitude";
            this.lbl_altitude.Size = new System.Drawing.Size(99, 19);
            this.lbl_altitude.TabIndex = 36;
            this.lbl_altitude.Text = "Altitude :";
            // 
            // txt_power
            // 
            this.txt_power.Enabled = false;
            this.txt_power.Location = new System.Drawing.Point(10, 299);
            this.txt_power.Name = "txt_power";
            this.txt_power.Size = new System.Drawing.Size(106, 30);
            this.txt_power.TabIndex = 39;
            // 
            // lbl_power
            // 
            this.lbl_power.AutoSize = true;
            this.lbl_power.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_power.ForeColor = System.Drawing.Color.Blue;
            this.lbl_power.Location = new System.Drawing.Point(6, 275);
            this.lbl_power.Name = "lbl_power";
            this.lbl_power.Size = new System.Drawing.Size(72, 19);
            this.lbl_power.TabIndex = 38;
            this.lbl_power.Text = "Power :";
            // 
            // txt_powerIndex
            // 
            this.txt_powerIndex.Enabled = false;
            this.txt_powerIndex.Location = new System.Drawing.Point(10, 376);
            this.txt_powerIndex.Name = "txt_powerIndex";
            this.txt_powerIndex.Size = new System.Drawing.Size(106, 30);
            this.txt_powerIndex.TabIndex = 41;
            // 
            // lbl_powerIndex
            // 
            this.lbl_powerIndex.AutoSize = true;
            this.lbl_powerIndex.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_powerIndex.ForeColor = System.Drawing.Color.Blue;
            this.lbl_powerIndex.Location = new System.Drawing.Point(6, 352);
            this.lbl_powerIndex.Name = "lbl_powerIndex";
            this.lbl_powerIndex.Size = new System.Drawing.Size(117, 19);
            this.lbl_powerIndex.TabIndex = 40;
            this.lbl_powerIndex.Text = "LRB and PI :";
            // 
            // Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1352, 705);
            this.Controls.Add(this.pan_createButton);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.pan_fileInformation);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.IsMdiContainer = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Dashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dashboard";
            this.pan_fileInformation.ResumeLayout(false);
            this.groupBox_summaryData.ResumeLayout(false);
            this.groupBox_summaryData.PerformLayout();
            this.groupBox_speedUnit.ResumeLayout(false);
            this.groupBox_speedUnit.PerformLayout();
            this.groupBox_headerData.ResumeLayout(false);
            this.groupBox_headerData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.pan_createButton.ResumeLayout(false);
            this.groupBox_sMode.ResumeLayout(false);
            this.groupBox_sMode.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel pan_fileInformation;
        private System.Windows.Forms.GroupBox groupBox_headerData;
        private System.Windows.Forms.Label lbl_intervalValue;
        private System.Windows.Forms.Label lbl_lengthValue;
        private System.Windows.Forms.Label lbl_startTimeValue;
        private System.Windows.Forms.Label lbl_dateValue;
        private System.Windows.Forms.Label lbl_sModeValue;
        private System.Windows.Forms.Label lbl_versionValue;
        private System.Windows.Forms.Label lbl_interval;
        private System.Windows.Forms.Label lbl_length;
        private System.Windows.Forms.Label lbl_startTime;
        private System.Windows.Forms.Label lbl_date;
        private System.Windows.Forms.Label lbl_sMode;
        private System.Windows.Forms.Label lbl_version;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.GroupBox groupBox_speedUnit;
        private System.Windows.Forms.RadioButton radioButton_milePerHour;
        private System.Windows.Forms.RadioButton radioButton_kmPerHour;
        private System.Windows.Forms.GroupBox groupBox_summaryData;
        private System.Windows.Forms.Label lbl_maxAltitude;
        private System.Windows.Forms.Label lbl_avgAltitude;
        private System.Windows.Forms.Label lbl_maxPower;
        private System.Windows.Forms.Label lbl_avgPower;
        private System.Windows.Forms.Label lbl_minHeartRate;
        private System.Windows.Forms.Label lbl_maxHeartRate;
        private System.Windows.Forms.Label lbl_avgHeartRate;
        private System.Windows.Forms.Label lbl_maxSpeed;
        private System.Windows.Forms.Label lbl_avgSpeed;
        private System.Windows.Forms.Label lbl_maxAltitudeValue;
        private System.Windows.Forms.Label lbl_avgAltitudeValue;
        private System.Windows.Forms.Label lbl_maxPowerValue;
        private System.Windows.Forms.Label lbl_avgPowerValue;
        private System.Windows.Forms.Label lbl_minHeartRateValue;
        private System.Windows.Forms.Label lbl_maxHeartRateValue;
        private System.Windows.Forms.Label lbl_avgHeartRateValue;
        private System.Windows.Forms.Label lbl_maxSpeedValue;
        private System.Windows.Forms.Label lbl_avgSpeedValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn HeartRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Speed;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cadence;
        private System.Windows.Forms.DataGridViewTextBoxColumn Altitude;
        private System.Windows.Forms.DataGridViewTextBoxColumn PowerInWatts;
        private System.Windows.Forms.DataGridViewTextBoxColumn PowerBalanceAndPedallingIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn Time;
        private System.Windows.Forms.Label lbl_totalDistanceValue;
        private System.Windows.Forms.Label lbl_totalDistance;
        private System.Windows.Forms.Panel pan_createButton;
        private System.Windows.Forms.Button btn_graphReport;
        private System.Windows.Forms.GroupBox groupBox_sMode;
        private System.Windows.Forms.TextBox txt_speed;
        private System.Windows.Forms.Label lbl_speed;
        private System.Windows.Forms.TextBox txt_powerIndex;
        private System.Windows.Forms.Label lbl_powerIndex;
        private System.Windows.Forms.TextBox txt_power;
        private System.Windows.Forms.Label lbl_power;
        private System.Windows.Forms.TextBox txt_altitude;
        private System.Windows.Forms.Label lbl_altitude;
        private System.Windows.Forms.TextBox txt_cadence;
        private System.Windows.Forms.Label lbl_cadence;
    }
}