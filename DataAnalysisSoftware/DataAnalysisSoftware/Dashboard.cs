﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataAnalysisSoftware
{
    public partial class Dashboard : Form
    {

        //variable declaration
        //can be used everywhere in this class
        string fileName;
        string singleLine;
        string sModeValue;
        string startTimeValue = "00:00:00.000";
        string intervalValue = "0";
        int[] heartRateArray;
        int[] speedArray;
        int[] cadenceArray;
        int[] altitudeArray;
        int[] powerInWattsArray;
        int totalItems;
        double avgSpeed;
        Boolean sModeSpeed = true;

        public Dashboard(string fileName)
        {
            this.fileName = fileName;
            InitializeComponent();
            //calling method to load all necessary information
            LoadHeaderData();
            LoadHRData();
            LoadSummaryData();
            LoadSModeInformation();
        }

        //method to load header data
        public void LoadHeaderData()
        {
            //file reader object
            StreamReader fileReader = new StreamReader(fileName);
            //reading each line of the file
            while ((singleLine = fileReader.ReadLine()) != null)
            {
                //splitting the line on certain keyword
                //and displaying obtained value in necessary form components
                if (singleLine.Contains("Version"))
                {
                    string versionValue = singleLine.Split('=').Last();
                    lbl_versionValue.Text = versionValue;
                }
                if (singleLine.Contains("SMode"))
                {
                    sModeValue = singleLine.Split('=').Last();
                    lbl_sModeValue.Text = sModeValue;
                }
                if (singleLine.Contains("Date"))
                {
                    string dateValue = singleLine.Split('=').Last();
                    //substring method to get year, month and day
                    string year = dateValue.Substring(0, 4);
                    string month = dateValue.Substring(4, 2);
                    string day = dateValue.Substring(6, 2);
                    string formattedDateValue = year + "-" + month + "-" + day;
                    lbl_dateValue.Text = formattedDateValue;
                }
                if (singleLine.Contains("StartTime"))
                {
                    startTimeValue = singleLine.Split('=').Last();
                    lbl_startTimeValue.Text = startTimeValue;
                }
                if (singleLine.Contains("Length"))
                {
                    string lengthValue = singleLine.Split('=').Last();
                    lbl_lengthValue.Text = lengthValue;
                }
                if (singleLine.Contains("Interval"))
                {
                    intervalValue = singleLine.Split('=').Last();
                    lbl_intervalValue.Text = intervalValue;
                }
            }
        }

        //method to load HRData
        public void LoadHRData()
        {
            //converting string time to DateTime type
            DateTime formattedStartTimeValue = DateTime.ParseExact(startTimeValue, 
                "HH:mm:ss.FFF", CultureInfo.InvariantCulture);
            int formattedIntervalValue = Convert.ToInt32(intervalValue);
            //read all file contents
            string fileContent = File.ReadAllText(fileName);
            //certain element to find
            string toFind = "[HRData]";
            //index of searched element
            int index = fileContent.IndexOf(toFind);
            //remaining file content after ommitting searched element
            string formattedFileContent = fileContent.Substring(index + 10, 
                fileContent.Length - (index + 10));
            //string reader to read string content
            StringReader stringReader = new StringReader(formattedFileContent);
            int row = 0;
            //reading each line of the string
            while ((singleLine = stringReader.ReadLine()) != null)
            {
                //splitting the line on certain keyword
                //and displaying obtained value in necessary form components
                string[] columnData = singleLine.Split('\t');
                dataGridView.Rows.Add();
                for (int i = 0; i < columnData.Length; i++)
                {
                    dataGridView[i, row].Value = columnData[i];
                }
                row++;
            }
            int totalRows = dataGridView.RowCount;
            int rowIndex = 6;
            for(int columnIndex = 0; columnIndex < totalRows; columnIndex++)
            {
                //adding time value in each column
                dataGridView[rowIndex, columnIndex].Value = formattedStartTimeValue.ToLongTimeString();
                formattedStartTimeValue = formattedStartTimeValue.AddSeconds(formattedIntervalValue);
            }
        }

        //method to load summary data
        public void LoadSummaryData()
        {
            //list variable declaration
            List<int> heartRate = new List<int>();
            List<int> speed = new List<int>();
            List<int> cadence = new List<int>();
            List<int> altitude = new List<int>();
            List<int> powerInWatts = new List<int>();
            foreach (DataGridViewRow row in dataGridView.Rows)
            {
                //adding each data of every column to corresponding list  
                heartRate.Add(int.Parse(row.Cells["HeartRate"].Value.ToString()));
                speed.Add(int.Parse(row.Cells["Speed"].Value.ToString()));
                cadence.Add(int.Parse(row.Cells["Cadence"].Value.ToString()));
                altitude.Add(int.Parse(row.Cells["Altitude"].Value.ToString()));
                powerInWatts.Add(int.Parse(row.Cells["PowerInWatts"].Value.ToString()));
            }
            //converting list to array
            heartRateArray = heartRate.ToArray();
            speedArray = speed.ToArray();
            cadenceArray = cadence.ToArray();
            altitudeArray = altitude.ToArray();
            powerInWattsArray = powerInWatts.ToArray();
            //calling methods that calculates summary data
            CalculateSpeed();
            CalculateHeartRate();
            CalculatePower();
            CalculateAltitude();
            CalculateTotalDistanceCovered();
        }

        //method to determine datagridview column analysing the SMode of file
        public void LoadSModeInformation()
        {
            //default value for every text field
            txt_speed.Text = "True";
            txt_cadence.Text = "True";
            txt_altitude.Text = "True";
            txt_power.Text = "True";
            txt_powerIndex.Text = "True";
            char[] sModeCharacter = new char[sModeValue.Length];
            for (int i = 0; i < sModeValue.Length; i++)
            {
                //reading and storing each character of SMode value
                sModeCharacter[i] = sModeValue[i];
            }
            //columns to hide based on SMode format
            //speed
            if (sModeCharacter[0] == '0')
            {
                sModeSpeed = false;
                txt_speed.Text = "False";
                dataGridView.Columns["Speed"].Visible = false;
                lbl_totalDistanceValue.Text = " ";
                lbl_avgSpeedValue.Text = " ";
                lbl_maxSpeedValue.Text = " ";
            }
            //cadence
            if (sModeCharacter[1] == '0')
            {
                txt_cadence.Text = "False";
                dataGridView.Columns["Cadence"].Visible = false;
            }
            //altitude
            if (sModeCharacter[2] == '0')
            {
                txt_altitude.Text = "False";
                dataGridView.Columns["Altitude"].Visible = false;
            }
            //power in watts
            if (sModeCharacter[3] == '0')
            {
                txt_power.Text = "False";
                dataGridView.Columns["PowerInWatts"].Visible = false;
            }
            //power balance and pedalling index
            if (sModeCharacter[4] == '0')
            {
                txt_powerIndex.Text = "False";
                dataGridView.Columns["PowerBalanceAndPedallingIndex"].Visible = false;
            }
        }

        //method to calculate required speed information
        public void CalculateSpeed()
        {
            totalItems = speedArray.Length;
            int totalSpeed = 0;
            for(int i = 0; i < totalItems; i++)
            {
                //total speed
                totalSpeed = totalSpeed + speedArray[i];
            }
            //average speed
            avgSpeed = Math.Round((double) totalSpeed / totalItems, 2);
            lbl_avgSpeedValue.Text = avgSpeed.ToString() + " " + "km/h";
            //maximum speed
            int maxSpeed = speedArray.Max();
            lbl_maxSpeedValue.Text = maxSpeed.ToString() + " " + "km/h";
        }

        //method to calculate required heart rate information
        public void CalculateHeartRate()
        {
            totalItems = heartRateArray.Length;
            int totalHeartRate = 0;
            for(int i = 0; i < totalItems; i++)
            {
                //total heart rate
                totalHeartRate = totalHeartRate + heartRateArray[i];
            }
            //average heart rate
            int avgHeartRate = totalHeartRate / totalItems;
            lbl_avgHeartRateValue.Text = avgHeartRate.ToString() + " " + "bpm";
            //maximum heart rate
            int maxHeartRate = heartRateArray.Max();
            lbl_maxHeartRateValue.Text = maxHeartRate.ToString() + " " + "bpm";
            //minimum heart rate
            int minHeartRate = heartRateArray.Min();
            lbl_minHeartRateValue.Text = minHeartRate.ToString() + " " + "bpm";
        }

        //method to calculate required power information
        public void CalculatePower()
        {
            totalItems = powerInWattsArray.Length;
            int totalPower = 0;
            for (int i = 0; i < totalItems; i++)
            {
                //total power
                totalPower = totalPower + powerInWattsArray[i];
            }
            //average power
            int avgPower = totalPower / totalItems;
            lbl_avgPowerValue.Text = avgPower.ToString() + " " + "watts";
            //maximum power
            int maxPower = powerInWattsArray.Max();
            lbl_maxPowerValue.Text = maxPower.ToString() + " " + "watts";
        }

        //method to calculate required altitude information
        public void CalculateAltitude()
        {
            totalItems = altitudeArray.Length;
            int totalAltitude = 0;
            for (int i = 0; i < totalItems; i++)
            {
                //total altitude
                totalAltitude = totalAltitude + altitudeArray[i];
            }
            //average altitude
            double avgAltitude = Math.Round((double) totalAltitude / totalItems, 2);
            lbl_avgAltitudeValue.Text = avgAltitude.ToString() + " " + "m/ft";
            //maximum altitude
            int maxAltitude = altitudeArray.Max();
            lbl_maxAltitudeValue.Text = maxAltitude.ToString() + " " + "m/ft";
        }


        //method to calculate distance covered
        public void CalculateTotalDistanceCovered()
        {
            string startTime = startTimeValue;
            //total seconds from start time
            double totalSeconds = TimeSpan.Parse(startTime).TotalSeconds;
            //calculating distance covered
            double totalDistance = Math.Round((double)(((avgSpeed / 60) / 60) * totalSeconds), 2);
            lbl_totalDistanceValue.Text = totalDistance.ToString() + " " + "km/h";
        }

        //event on radio button click
        private void radioButton_kmPerHour_Click(object sender, EventArgs e)
        {
            //radio button enable and disable as per requirement
            radioButton_kmPerHour.Enabled = false;
            radioButton_milePerHour.Enabled = true;
            int rowIndex = 1;
            totalItems = speedArray.Length;
            for(int columnIndex = 0; columnIndex < totalItems; columnIndex++)
            {
                //speed in km/h
                dataGridView[rowIndex, columnIndex].Value = speedArray[columnIndex];
            }
            //speed and distance calculation as per SMode information
            if (sModeSpeed)
            {
                totalItems = speedArray.Length;
                int totalSpeed = 0;
                for (int i = 0; i < totalItems; i++)
                {
                    totalSpeed = totalSpeed + speedArray[i];
                }
                double avgSpeed = Math.Round((double)totalSpeed / totalItems, 2);
                lbl_avgSpeedValue.Text = avgSpeed.ToString() + " " + "km/h";
                int maxSpeed = speedArray.Max();
                lbl_maxSpeedValue.Text = maxSpeed.ToString() + " " + "km/h";
                string startTime = startTimeValue;
                double totalSeconds = TimeSpan.Parse(startTime).TotalSeconds;
                double totalDistance = Math.Round((double)(((avgSpeed / 60) / 60) * totalSeconds), 2);
                lbl_totalDistanceValue.Text = totalDistance.ToString() + " " + "km/h";
            }
        }

        //event on radio button click
        private void radioButton_milePerHour_Click(object sender, EventArgs e)
        {
            //radio button enable and disable as per requirement
            radioButton_milePerHour.Enabled = false;
            radioButton_kmPerHour.Enabled = true;
            int rowIndex = 1;
            totalItems = speedArray.Length;
            double[] kmToMiles = new double[totalItems];
            for(int columnIndex = 0; columnIndex < totalItems; columnIndex++)
            {
                //speed in mph
                kmToMiles[columnIndex] = Math.Round(speedArray[columnIndex] * 0.621371, 2);
                dataGridView[rowIndex, columnIndex].Value = kmToMiles[columnIndex];
            }
            //speed and distance calculation as per SMode information
            if (sModeSpeed)
            {
                totalItems = kmToMiles.Length;
                double totalSpeed = 0;
                for (int i = 0; i < totalItems; i++)
                {
                    totalSpeed = totalSpeed + kmToMiles[i];
                }
                double avgSpeed = Math.Round((double)totalSpeed / totalItems, 2);
                lbl_avgSpeedValue.Text = avgSpeed.ToString() + " " + "mph";
                double maxSpeed = kmToMiles.Max();
                lbl_maxSpeedValue.Text = maxSpeed.ToString() + " " + "mph";
                string startTime = startTimeValue;
                double totalSeconds = TimeSpan.Parse(startTime).TotalSeconds;
                double totalDistance = Math.Round((double)(((avgSpeed / 60) / 60) * totalSeconds), 2);
                lbl_totalDistanceValue.Text = totalDistance.ToString() + " " + "mph";
            }
        }

        //event triggered on graph generate button click
        private void btn_graphReport_Click(object sender, EventArgs e)
        {
            //calling graph class constructor
            Graph graph = new Graph(heartRateArray,speedArray,cadenceArray,altitudeArray, 
                powerInWattsArray);
            graph.ShowDialog();
        }
    }
}
